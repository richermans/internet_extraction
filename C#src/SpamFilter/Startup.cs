﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SpamFilter.Startup))]
namespace SpamFilter
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
