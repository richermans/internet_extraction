﻿using BayesClassifier;
using SpamFilter.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SpamFilter.Controllers
{
    public class HomeController : Controller
    {
        
        static Classifier classifier;

        public ActionResult Index()
        {
            SpamHamEntities dbContext = new SpamHamEntities();

            int countSpam = (from spam in dbContext.Spam select spam).Count();
            int countHam = (from ham in dbContext.Ham select ham).Count();

            ViewBag.CountHam = countHam;
            ViewBag.CountSpam = countSpam;

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Authorize]
        public ActionResult Statistics()
        {

            SpamHamEntities dbContext = new SpamHamEntities();

            int countSpam = (from spam in dbContext.Spam select spam).Count();
            int countHam = (from ham in dbContext.Ham select ham).Count();

                var spamQuery = (from spam in dbContext.Spam
                                orderby spam.Count descending
                                select spam).Take(20);

                var hamQuery = (from ham in dbContext.Ham
                                 orderby ham.Count descending
                                 select ham).Take(20);

                List<Spam> top20SpamList = new List<Spam>();
                List<Ham> top20HamList = new List<Ham>();

                foreach (Spam s in spamQuery)
                {
                    top20SpamList.Add(s);
                }

                foreach (Ham h in hamQuery)
                {
                    top20HamList.Add(h);
                }

            ViewBag.Spamlist = top20SpamList;
            ViewBag.Hamlist = top20HamList;
            ViewBag.CountHam = countHam;
            ViewBag.CountSpam = countSpam;

            return View();
        }

        [Authorize]
        public ActionResult CheckMail()
        {

            classifier = new Classifier();

            return View();
        }

        [HttpPost]
        public ActionResult CalculateSpam()
        {

            string emailData = Request.Unvalidated.Form["emailText"];

            emailData = emailData.ToLower();

            // convert string to stream
            byte[] byteArray = Encoding.UTF8.GetBytes(emailData);
            MemoryStream stream = new MemoryStream(byteArray);

            // convert stream to string
            StreamReader reader = new StreamReader(stream);

            Dictionary<String,Double> result = classifier.Classify(reader);

            double spamValue;
            double hamValue;

            result.TryGetValue("Spam", out spamValue);
            result.TryGetValue("Ham", out hamValue);

            string resultMessage = null;

            int compareResult = spamValue.CompareTo(hamValue);

            if (compareResult < 0)
            {
                resultMessage = "I guess that your E-Mail is probably Ham!";
            } 
            else if (compareResult >0)
            {
                resultMessage = "I guess that your E-Mail is probably Spam!";
            }
            else if (compareResult == 0)
            {
                resultMessage = "I am sorry, but I can not decide whether it is Spam or Ham!";
            }

            ViewBag.SpamValue = spamValue;
            ViewBag.HamValue = hamValue;
            ViewBag.ResultMessage = resultMessage;

            return View();
        }

    }
}