﻿using BayesClassifier;
using DatabaseLoader.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLoader
{
    public class Program
    {
        Classifier classifier;
        ICategory spamCategory;
        ICategory hamCategory;

        static void Main(string[] args)
        {
            Console.WriteLine("start");

            Program program = new Program();

            program.loadDataInMemory();
            program.writeToDatabase();

            Console.WriteLine("fertig");
            Console.ReadLine();
        }

        public Program()
        {
            classifier = new Classifier();
        }

        public void loadDataInMemory()
        {
            Console.WriteLine("load data in memory");

            string indexfile = "D:/spamdata/trec06p/trec06p/full/index";

            List<String> spamFileList = new List<String>();
            List<String> hamFileList = new List<String>();

            TextReader indexReader = new StreamReader(indexfile);

            Console.WriteLine("load index file");

            string data = indexReader.ReadLine();
            while (data != null)
            {
                string[] tempData = data.Split(' ');

                if (tempData[0].Equals("ham"))
                {
                    hamFileList.Add(tempData[1]);
                }
                else if (tempData[0].Equals("spam"))
                {
                    spamFileList.Add(tempData[1]);
                }

                data = indexReader.ReadLine();
            }

            indexReader.Close();

            Console.WriteLine("finished index file load");

            Console.WriteLine("start teach ham data");

            foreach (string hamPath in hamFileList)
            {
                classifier.TeachCategory("Ham", new System.IO.StreamReader(hamPath));
            }

            Console.WriteLine("finished teach ham data");

            Console.WriteLine("start teach spam data");

            foreach (string spamPath in spamFileList)
            {
                classifier.TeachCategory("Spam", new System.IO.StreamReader(spamPath));
            }

            Console.WriteLine("finished teach spam data");
     

            SortedDictionary<string, ICategory> categories = classifier.getCategories();

            categories.TryGetValue("Spam",out spamCategory);
            categories.TryGetValue("Ham", out hamCategory);

        }

        public void writeToDatabase()
        {

            Console.WriteLine("start write to database");

            Console.WriteLine("prepare orm");

            SortedDictionary<string, PhraseCount> spamPhrases = spamCategory.getPhrases();
            SortedDictionary<string, PhraseCount> hamPhrases = hamCategory.getPhrases();

            Entities dbContext = new Entities();

            Console.WriteLine("start spam orm");

            int i = 0;
            foreach (KeyValuePair<string, PhraseCount> p in spamPhrases)
            {
                
                Console.WriteLine("{0} = {1}",
                p.Key,
                p.Value.Count);
                 

                Spam spamEntity = new Spam();
                spamEntity.Id = i;
                spamEntity.Word=p.Key;
                spamEntity.Count=p.Value.Count;
                dbContext.Spam.Add(spamEntity);
                i++;
            }

            Console.WriteLine("finished spam orm");

            Console.WriteLine("start ham orm");

            int j = 0;
            foreach (KeyValuePair<string, PhraseCount> q in hamPhrases)
            {
                Console.WriteLine("{0} = {1}",
                q.Key,
                q.Value.Count);

                Ham hamEntity = new Ham();
                hamEntity.Id = j;
                hamEntity.Word = q.Key;
                hamEntity.Count = q.Value.Count;
                dbContext.Ham.Add(hamEntity);
                j++;
            }

            Console.WriteLine("finished ham orm");

            Console.WriteLine("start to save entities in db");

            dbContext.SaveChanges();
            
        }
    }
}
