\select@language {english}
\contentsline {chapter}{List of Figures}{III}{Doc-Start}
\contentsline {chapter}{Abstract}{2}{chapter*.2}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.3}
\contentsline {section}{\numberline {1.1}Framework analysis}{4}{section.4}
\contentsline {chapter}{\numberline {2}Fundamentals}{6}{chapter.5}
\contentsline {section}{\numberline {2.1}Supervised Learning}{6}{section.6}
\contentsline {section}{\numberline {2.2}Corpus}{7}{section.7}
\contentsline {chapter}{\numberline {3}Classifier}{8}{chapter.8}
\contentsline {section}{\numberline {3.1}Classification}{8}{section.9}
\contentsline {section}{\numberline {3.2}SVM}{8}{section.10}
\contentsline {subsection}{\numberline {3.2.1}PegaSoS}{9}{subsection.11}
\contentsline {subsection}{\numberline {3.2.2}DCD}{9}{subsection.12}
\contentsline {section}{\numberline {3.3}Bayesian}{9}{section.13}
\contentsline {subsection}{\numberline {3.3.1}Naive}{9}{subsection.14}
\contentsline {subsection}{\numberline {3.3.2}MultiNominal}{10}{subsection.15}
\contentsline {chapter}{\numberline {4}Result}{12}{chapter.16}
\contentsline {section}{\numberline {4.1}Statistics}{12}{section.17}
\contentsline {subsection}{\numberline {4.1.1}PegaSoS Results}{12}{subsection.18}
\contentsline {subsection}{\numberline {4.1.2}DCD Results}{12}{subsection.23}
\contentsline {subsection}{\numberline {4.1.3}Naive Bayes Results}{12}{subsection.28}
\contentsline {section}{\numberline {4.2}Comparison}{12}{section.33}
\contentsline {chapter}{\numberline {5}Prototype}{19}{chapter.34}
\contentsline {section}{\numberline {5.1}Used Implementation}{19}{section.35}
\contentsline {section}{\numberline {5.2}Architecture}{20}{section.37}
\contentsline {section}{\numberline {5.3}User interface}{23}{section.39}
\contentsline {chapter}{\numberline {6}Conclusion}{26}{chapter.45}
\contentsline {chapter}{Bibliography}{28}{chapter*.46}
\contentsline {chapter}{Appendices}{i}{section*.47}
\contentsline {chapter}{\numberline {A}First}{ii}{Appendix.48}
\contentsline {section}{\numberline {A.1}sec}{ii}{section.49}
\contentsline {chapter}{\numberline {B}Next}{iii}{Appendix.50}
\contentsline {section}{\numberline {B.1}sec}{iii}{section.51}
