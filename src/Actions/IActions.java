package Actions;

import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.Classifier;

public interface IActions {
	
	public void doAction(ClassificationDataSet corpus,Classifier method);
		
	
}
