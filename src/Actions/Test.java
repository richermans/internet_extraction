package Actions;

import jsat.classifiers.CategoricalResults;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.Classifier;
import jsat.classifiers.DataPoint;

public class Test implements IActions {

	@Override
	public void doAction(ClassificationDataSet corpus, Classifier method) {
		int errors = 0;
		for (int i = 0; i < corpus.getSampleSize(); i++) {
			DataPoint dataPoint = corpus.getDataPoint(i);// It is important
															// not to mix these
															// up, the class has
															// been removed from
															// data points in
															// 'cDataSet'
			int truth = corpus.getDataPointCategory(i);// We can grab the true
															// category from the
															// data set

			// Categorical Results contains the probability estimates for each
			// possible target class value.
			// Classifiers that do not support probability estimates will mark
			// its prediction with total confidence.
			CategoricalResults predictionResults = method
					.classify(dataPoint);
			int predicted = predictionResults.mostLikely();
			if (predicted != truth)
				errors++;
			/*if(i==0 || i==1 || i==2)
			{System.out.println(i + "| True Class: " + truth + ", Predicted: "
					+ predicted + ", Confidence: "
					+ predictionResults.getProb(predicted));}*/

		}
		
		System.out.println(errors + " errors were made, " + 100.0 * errors
				/ corpus.getSampleSize() + "% error rate");
	}


}
