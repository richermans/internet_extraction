package Actions;

import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.ClassificationModelEvaluation;
import jsat.classifiers.Classifier;

public class CrossValidate implements IActions {

	@Override
	public void doAction(ClassificationDataSet corpus, Classifier method) {
		
		System.out.println(corpus.getNumNumericalVars() + " features");

		long totaltrainingtime=0,totalclassificatiotime=0;
		double errorrate=0;
		for (int i = 0; i < 3; i++) {

			// ture indicates that the data is sparse and zeros are not
			// important.
			// This is almost always the case when working with text data
			ClassificationModelEvaluation cme = new ClassificationModelEvaluation(
					method, corpus);

			cme.evaluateCrossValidation(10);
			System.out.println("Total Training Time: "
					+ cme.getTotalTrainingTime());
			System.out.println("Total Classification Time: "
					+ cme.getTotalClassificationTime());
			totaltrainingtime = totaltrainingtime + cme.getTotalTrainingTime();
			totalclassificatiotime = totalclassificatiotime
					+ cme.getTotalClassificationTime();

			System.out.println("Total Error rate: " + cme.getErrorRate() * 100);
			cme.prettyPrintConfusionMatrix();
			// }
			errorrate = errorrate + cme.getErrorRate() * 100;
		}
		System.out.println("\nTotal error rate AVG: " + errorrate / 3);
		System.out.println("Total classification time AVG: "
				+ totalclassificatiotime / 3);
		System.out.println("Total training time AVG: " + totaltrainingtime / 3);
		System.out.println("_______________________________________________");

	}

}
