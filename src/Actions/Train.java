package Actions;

import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.Classifier;

public class Train implements IActions {

	@Override
	public void doAction(ClassificationDataSet corpus, Classifier method) {
		System.out.println("Training Data set loaded in");
		System.out.println(corpus.getSampleSize() + " data point");
		System.out.println(corpus.getNumFeatures() + " Features");

		method.trainC(corpus);
	}

}
