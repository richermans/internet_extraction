/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Just a wrapper around File, to support the labels for the DatasetLoader
 *
 * @author Clonex
 */
public abstract class AbstractFile {

    private final List<File> filePaths;
    private final SpamHamEnum spam_ham;

    protected AbstractFile(File fileP, SpamHamEnum spam_ham) throws FileNotFoundException {
        if (fileP != null && fileP.exists()) {
            filePaths = new ArrayList<>();
            if (fileP.isDirectory()) {
                addFilesTOList(filePaths, fileP);
            } else {
                filePaths.add(fileP);
            }

            this.spam_ham = spam_ham;
        } else {
            throw new FileNotFoundException("File does not exist ! Check your Path!");
        }
    }
    
    private void addFilesTOList(List<File> fileList, File fileP){
        if (fileP.isFile()){
            fileList.add(fileP);
        }else {
            for (File fileF: fileP.listFiles()){
                addFilesTOList(fileList, fileF);
            }            
        }
    }

    public int getLabelValue() {
        return spam_ham.getValue();
    }

    /**
     * Returns the file if it's a file otherwise returns all Files in the
     * Directory
     *
     * @return
     */
    public List<File> getFilePaths() {
        return new ArrayList<File>() {
            {
                {
                    this.addAll(filePaths);
                };
            }
        };
    }

    public abstract void process();
    /*
     * Enum for Spam or ham
     */

    enum SpamHamEnum {

        SPAM(0), HAM(1);
        private int value;

        private SpamHamEnum(int val) {
            this.value = val;
        }

        public void setVal(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

}
