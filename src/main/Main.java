package main;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
	
	public static void main(String[] args) {
            try {
                //TODO: args are something like -h <hamfile/hamdir> -s <spamfile/spamdir> .. data team?
                Options.parseOptions(args);
            } catch (Exception ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, ex.getMessage());
            }
		
	}

}
