package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import jsat.classifiers.CategoricalData;
import jsat.text.ClassificationTextDataLoader;
import jsat.text.tokenizer.Tokenizer;
import jsat.text.wordweighting.WordWeighting;

//Extracts the given data ( Enron/ Trac )
public class DataSetLoader extends ClassificationTextDataLoader {

	private List<AbstractFile> abstractFiles;
	private File indexFile;
	private final Map<String, Integer> classKeys = new HashMap<String, Integer>();

	public DataSetLoader(final Tokenizer tokenizer,
			final WordWeighting weighting, final List<AbstractFile> sp) {
		super(tokenizer, weighting);
		this.abstractFiles = sp;
	}

	public DataSetLoader(Tokenizer tokenizer, WordWeighting weighting,
			File indexFile) {
		super(tokenizer, weighting);
		this.indexFile = indexFile;
		classKeys.put("spam", 0);
		classKeys.put("ham", 1);
	}

	@Override
	protected void setLabelInfo() {
		if (abstractFiles != null) {
			labelInfo = new CategoricalData(abstractFiles.size());
			for (AbstractFile file : this.abstractFiles) {
				labelInfo.setOptionName(file.toString(), file.getLabelValue());
			}
		}
		if (indexFile != null) {
			labelInfo = new CategoricalData(2);
			for (Entry<String, Integer> entry : classKeys.entrySet())
				labelInfo.setOptionName(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public void initialLoad() {
		// read E-Mail data and

		if (abstractFiles != null) {
			for (AbstractFile a : abstractFiles) { // Returns String for further
				// processing
				List<File> tempFiles = a.getFilePaths();
				// Iterate over the whole directory if necessary
				for (File temFile : tempFiles) {
					
				}
			}

		}

		if (indexFile != null) {
			BufferedReader br = null;
			try {
				char[] buffer = new char[8192 * 2];
				// Trec index file format is <label> <relativePathToFile>
				br= new BufferedReader(
						new FileReader(indexFile), 8192 * 2);
				String line;
				StringBuilder dataFileText = new StringBuilder();

				while ((line = br.readLine()) != null) {
					String[] split = line.split("\\s+");// split one white space

					File dataFile = new File(indexFile.getParentFile(),
							split[1]);

					if (dataFile.exists()) {
						FileReader dataFileReader = new FileReader(dataFile);

						dataFileText.delete(0, dataFileText.length());

						int readIn;
						while ((readIn = dataFileReader.read(buffer)) >= 0) {
							dataFileText.append(buffer, 0, readIn);
						}

						dataFileReader.close();
						addOriginalDocument(dataFileText.toString(),
								classKeys.get(split[0]));
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}finally{
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}

}
