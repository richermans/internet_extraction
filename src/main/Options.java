/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.File;
import java.io.IOError;
import java.util.ArrayList;
import java.util.List;

import Actions.CrossValidate;
import Actions.IActions;
import Actions.Test;
import Actions.Train;
import jsat.classifiers.Classifier;
import jsat.classifiers.bayesian.NaiveBayes;
import jsat.classifiers.knn.DANN;
import jsat.classifiers.svm.Pegasos;
import jsat.classifiers.svm.PlatSMO;
import jsat.classifiers.svm.SBP;
import jsat.classifiers.svm.SupportVectorLearner;
import jsat.classifiers.svm.SupportVectorLearner.CacheMode;
import jsat.distributions.kernels.RBFKernel;

/**
 * 
 * @author Clonex
 */
public class Options {

	public enum OPTS {
		ham("-h", "<ham File/ham directory>"), 
		spam("-s",
				"<spam File/spam directory>"), 
		index("-i",
				" <index file>"),
		// train ("-train", "usage: -train <training data set>"),
		testFile("-testFile", " -testfile <test data set>"),
		test("--test","corpus will be tested"),
		train("--train","says that the corpus needs to be trained"),
		crossvalidate("--cross","Trains the input and crossvalidates it"),
		bayes("--bayes",
				"enables bayes classification"), 
		svm("--svm",
				"enables svm classification");

		public String command, desc;

		private OPTS(String command, String desc) {
			this.command = command;
			this.desc = desc;
		}

	}

	private static List<AbstractFile> spamHamFiles;
	private static File testFile;

	public static void parseOptions(String args[]) throws Exception {
		spamHamFiles = new ArrayList<>();
		Classifier classifier = null;
		if (args.length < 2) {
			printDefaultUsageMessage();
		}
		ClassifyExecutor executor = null;
		ArrayList<IActions> actions = new ArrayList<>();
		for (int i = 0; i < args.length; i++) {
			String current = args[i];
			for (OPTS opt : OPTS.values()) {
				if (opt.command.equalsIgnoreCase(current)) {
					i++;
					switch (opt) {
					case ham:
						if (i >= args.length) {
							
							throw new Exception("Argument behind " + opt.command
									+ " wrong! expected a filepath");
						}
						spamHamFiles.add(new HamFile(new File(args[i])));
						break;

					case spam:
						if (i >= args.length) {
							throw new Exception("Argument behind " + opt.command
									+ " wrong! expected a filepath");
						}
						String fileDirectory = args[i];
						spamHamFiles.add(new SpamFile(new File(fileDirectory)));
						break;
					case testFile:
						if (i >= args.length) {
							throw new Exception("Argument behind " + opt.command
									+ " wrong! expected a filepath");
						}
						testFile = new File(args[i]);
						if (testFile == null) {
							throw new Exception("Wrong File path!");
						}
						if (!testFile.exists()) {
							throw new Exception("File does not exist!");
						}
						if (testFile.isDirectory()) {
							throw new Exception(
									"We do not support directory as given File!");
						}
						//TODO:implement test cases
						break;
					case bayes:
						if (classifier == null)
							classifier = new NaiveBayes();
						break;
					case svm:
						if (classifier == null)
							classifier = new Pegasos();
						break;
					case index:
						if (i >= args.length) {
							throw new Exception("Argument behind " + opt.command
									+ " wrong!");
						}
						executor = new ClassifyExecutor(new File(args[i]));
						break;
					case test:
						actions.add(new Test());
						break;
					case train:
						actions.add(new Train());
						break;
					case crossvalidate:
						actions.add(new CrossValidate());
						break;
					default:
						printDefaultUsageMessage();
						break;

					}
				}

			}
		}
		if (executor == null)
			executor = new ClassifyExecutor(spamHamFiles);
		if(classifier==null)
			throw new Exception("You need to specify a classifier!");
		executor.setClassifier(classifier);
		if(actions.size()>0)
			executor.setActions(actions);
		executor.execute();
		
	}

	private static void printDefaultUsageMessage() {
		System.out.println("Input Error, Usage:");
		for (OPTS op : OPTS.values()) {
			System.out.println(op.command + " " + op.desc);
		}
		System.out.println("You can either use -h AND -s or an index file with -i");
	}

}
