package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import Actions.CrossValidate;
import Actions.IActions;
import jsat.DataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.ClassificationModelEvaluation;
import jsat.classifiers.Classifier;
import jsat.classifiers.bayesian.NaiveBayes;
import jsat.linear.Vec;
import jsat.text.tokenizer.NaiveTokenizer;
import jsat.text.tokenizer.Tokenizer;
import jsat.text.wordweighting.TfIdf;
import jsat.text.wordweighting.WordWeighting;
import main.AbstractFile.SpamHamEnum;

/**
 * This Class provides a abstract Executor, which does the general classifiying
 * job, including parsing the data from the given File and further on running
 * several classifying techniques on it
 * 
 * @author richman
 * 
 */
public final class ClassifyExecutor {

	private final ArrayBlockingQueue<Runnable> workQuene = new ArrayBlockingQueue<>(
			THREAD_ARGS.maximumPoolSize.get());
	private final ExecutorService executorService = new ThreadPoolExecutor(
			THREAD_ARGS.corePoolSize.get(), THREAD_ARGS.maximumPoolSize.get(),
			THREAD_ARGS.keepAliveTime.get(), TimeUnit.MINUTES, workQuene);
	private final DataSetLoader loader;
	private Classifier classifier;
	private File outPutPath;
	private List<IActions> actionsToDo;
	private static String DEFAULTPATH = "outputDATA.hrsn";
	private static List<IActions> defaultActions = new ArrayList<IActions>() {
		{
			{
				this.add(new CrossValidate());
			}
		}
	};

	/**
	 * 
	 * @param sp
	 *            List, which indicates weather the given file consists of just
	 *            spam or just ham files or both
	 */
	public ClassifyExecutor(final List<AbstractFile> sp) {
		this.loader = new DataSetLoader(new NaiveTokenizer(), new TfIdf(), sp);
		this.outPutPath = new File(DEFAULTPATH);
		this.actionsToDo = defaultActions;
	}

	public ClassifyExecutor(final File index) {
		this.loader = new DataSetLoader(new NaiveTokenizer(), new TfIdf(),
				index);
		this.outPutPath = new File(DEFAULTPATH);
		this.actionsToDo = defaultActions;
	}

	public void setOutputPath(File p) {
		this.outPutPath = p;
	}

	/**
	 * 
	 * 
	 * @param tokenizer
	 * @param weight
	 *            the average weight a word ( multiplication ) is given
	 * @param sp
	 *            is the List wether the given file consists of just spam or
	 *            just ham files or both
	 */
	public ClassifyExecutor(final Tokenizer tokenizer,
			final WordWeighting weight, final List<AbstractFile> sp) {
		this.loader = new DataSetLoader(tokenizer, weight, sp);
		this.outPutPath = new File(DEFAULTPATH);
		this.actionsToDo = defaultActions;
	}

	public void setClassifier(Classifier classifier) {
		if (classifier != null)
			this.classifier = classifier;
	}

	public void execute() {
		// For each given dataset execute this
		System.out.println("Starting classification Jobs!");
		ClassificationDataSet dataset = loader.getDataSet();
		System.out.println("Datasets loaded! " + dataset.getDataPoints().size() + " Datapoints ");
		for (IActions action : actionsToDo) {
			action.doAction(dataset, classifier);
		}
		System.out.println("Finished classification Jobs!");

	}

	private boolean outputFile(File file,
			ClassificationModelEvaluation classificationModelEval) {
		try {
			if (file == null || classificationModelEval == null
					|| !file.exists()) {
				return false;
			}
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			classificationModelEval.getTotalClassificationTime();
			bw.write("classificaionTime: ");
			bw.write((int) classificationModelEval.getTotalClassificationTime());
			bw.write("\n");
			bw.write("ErrorRate: ");
			bw.write((int) classificationModelEval.getErrorRate());
			bw.write("\n");
			bw.write("ConfusionMatrix: ");
			double confusionMat[][] = classificationModelEval
					.getConfusionMatrix();
			if (confusionMat == null || confusionMat[0] == null) {
				bw.close();
				return false;
			}
			for (int i = 0; i < confusionMat[0].length; i++) {
				for (int j = 0; j < confusionMat.length; j++) {
					bw.write(confusionMat[i][j] + " ");
				}
				bw.write("\n");
			}
			bw.close();
		} catch (IOException ex) {
			Logger.getLogger(ClassifyExecutor.class.getName()).log(
					Level.SEVERE, null, ex);
			return false;
		}
		return true;
	}

	public static void setDefaultOutPut(String s) {
		DEFAULTPATH = s;
	}

	// Helper for threadpool arguments..change if necessary
	public enum THREAD_ARGS {

		corePoolSize(4), maximumPoolSize(20), keepAliveTime(10000);

		private Integer val;

		THREAD_ARGS(int s) {
			this.val = s;
		}

		public void setVal(int val) {
			this.val = val;
		}

		public int get() {
			return val;
		}

	}

	public void setActions(ArrayList<IActions> actions) {
		this.actionsToDo = actions;

	}

	public static void setDefaultActions(List<IActions> actions) {

	}

}
